<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use DateTimeInterface;
use Stringable;

/**
 * ApiFrInseeSireneUniteLegaleHistoriqueInterface interface file.
 * 
 * This represents a line into the StockUniteLegaleHistorique file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeSireneUniteLegaleHistoriqueInterface extends Stringable
{
	
	/**
	 * Gets un numéro d’identité de l’unité légale est attribué par
	 * l’Insee à toutes les personnes physiques ou morales inscrites au
	 * répertoire ainsi qu’à leurs établissements : le numéro Siren. Ce
	 * numéro unique est « attribué soit à l’occasion des demandes
	 * d’immatriculation au registre du commerce et des sociétés ou des
	 * déclarations effectuées au répertoire des métiers, soit à la
	 * demande d’administrations » (article R123-224 du code de commerce).
	 * Lors de sa création, une unité légale se voit attribuer un numéro
	 * Siren de 9 chiffres.
	 * 
	 * *Règles de gestion*
	 * Les entrepreneurs individuels, ou personnes physiques, conservent le
	 * même numéro Siren jusqu’à leur décès. Les sociétés, ou
	 * personnes morales, perdent la personnalité juridique au moment de la
	 * cessation de l’activité de l’entreprise. Si l’activité devait
	 * reprendre ultérieurement, un nouveau numéro Siren sera attribué. Les
	 * numéros d’identification sont uniques : lorsqu’un numéro Siren a
	 * été attribué, il ne peut pas être réutilisé et attribué à une
	 * nouvelle unité légale, même lorsque l’activité a cessé.
	 * 
	 * *Historique*
	 * Même si la mise en place du répertoire Sirene remonte à 1973, toutes
	 * les unités légales, y compris celles créées avant cette date,
	 * disposent d’un numéro Siren pour le secteur privé non agricole. En
	 * 1983, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus aux institutions et services de
	 * l’État et aux collectivités territoriales.
	 * En 1993, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus au secteur privé agricole.
	 * 
	 * Longueur : 9
	 * 
	 * @return string
	 */
	public function getSiren() : string;
	
	/**
	 * Gets cette variable désigne la date de fin de la période au cours de
	 * laquelle toutes les variables historisées de l'unité légale restent
	 * inchangées.
	 * La date de fin est calculée, elle est égale à la veille de la date de
	 * début de la période suivante dans l'ordre chronologique.
	 * Si la date de fin de la période est null, la période correspond à la
	 * situation courante de l'unité légale.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateFin() : ?DateTimeInterface;
	
	/**
	 * Gets date de début de la période au cours de laquelle toutes les
	 * variables historisées de l'entreprise restent inchangées. La date
	 * 1900-01-01 signifie : date non déterminée. dateDebut peut-être vide
	 * uniquement pour les unités purgées (cf variable unitePurgeeUniteLegale
	 * du fichier StockUniteLegale). La date de début de la période la plus
	 * ancienne ne correspond pas obligatoirement à la date de création de
	 * l'entreprise, certaines variables historisées pouvant posséder des
	 * dates de début soit au 1900-01-01, soit antérieures à la date de
	 * création.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateDebut() : ?DateTimeInterface;
	
	/**
	 * Gets le passage à l’état « Cessée » découle de la prise en
	 * compte d’une déclaration de cessation administrative.
	 * Pour les personnes morales, cela correspond au dépôt de la
	 * déclaration de disparition de la personne morale.
	 * Pour les personnes physiques, cela correspond soit à la prise en compte
	 * de la déclaration de cessation d’activité déposée par
	 * l’exploitant soit au décès de l’exploitant conformément à la
	 * réglementation. En dehors de ces cas, l’unité légale est toujours
	 * à l’état administratif « Active ».
	 * Pour les personnes morales, la cessation administrative est, en
	 * théorie, définitive, l’état administratif "Cessée" est
	 * irréversible.
	 * Cependant, il existe actuellement dans la base un certain nombre
	 * d’unités légales personnes morales avec un historique d'état
	 * présentant un état cessé entre deux périodes à l’état actif.
	 * Pour les personnes physiques, dans le cas où l’exploitant déclare la
	 * cessation de son activité, puis la reprend quelque temps plus tard, cet
	 * état est réversible. Il est donc normal d'avoir des périodes
	 * successives d'état actif puis cessé pour les personnes physiques. En
	 * règle générale, la première période d’historique d’une unité
	 * légale correspond à un etatAdministratifUniteLegale égal à « Active
	 * ».
	 * Toutefois, l'état administratif peut être à null (première date de
	 * début de l'état postérieure à la première date de début d'une
	 * autre variable historisée).
	 * 
	 * @return ?ApiFrInseeSireneEtatAdministratifInterface
	 */
	public function getEtatAdministratifUniteLegale() : ?ApiFrInseeSireneEtatAdministratifInterface;
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * EtatAdministratifUniteLegale a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementEtatAdministratifUniteLegale() : bool;
	
	/**
	 * Gets cette variable indique le libellé le nom de naissance pour une
	 * personne physique. Cette variable est à null pour les personnes
	 * morales.
	 * Le répertoire Sirene gère des caractères majuscules non accentués et
	 * les seuls caractères spéciaux tiret (-) et apostrophe.
	 * Le nom peut être à null (cas des unités purgées, première date de
	 * début du nom postérieure à la première date de début d'une autre
	 * variable historisée).
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getNomUniteLegale() : ?string;
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * nomUniteLegale a été modifiée par rapport à la période
	 * précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementNomUniteLegale() : bool;
	
	/**
	 * Gets le nom d’usage est celui que la personne physique a choisi
	 * d’utiliser.
	 * Cette variable est à null pour les personnes morales. Elle peut être
	 * également à null pour les personnes physiques.
	 * Le répertoire Sirene gère des caractères majuscules non accentués et
	 * les seuls caractères spéciaux tiret (-) et apostrophe.
	 * Cette variable est historisée.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getNomUsageUniteLegale() : ?string;
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * nomUsageUniteLegale a été modifiée a été modifiée par rapport à
	 * la période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementNomUsageUniteLegale() : bool;
	
	/**
	 * Gets cette variable désigne la raison sociale pour les personnes
	 * morales. Il s'agit du nom sous lequel est déclarée l'unité légale.
	 * Cette variable est à null pour les personnes physiques.
	 * Le répertoire Sirene gère des caractères majuscules non accentués
	 * avec caractères spéciaux (- & + @ ! ? * ° . % : € #).
	 * La dénomination peut parfois contenir la mention de la forme de la
	 * société (SA, SAS, SARL, etc.).
	 * 
	 * *Historique*
	 * Un travail de reconstitution des historiques de dénomination a été
	 * entrepris en 2005. Toutefois, avant cette date, des historiques de
	 * dénomination peuvent s’avérer incomplets. La dénomination peut
	 * être à null (cas des unités purgées, première date de début de la
	 * dénomination postérieure à la première date de début d'une autre
	 * variable historisée).
	 * 
	 * Longueur : 120
	 * 
	 * @return ?string
	 */
	public function getDenominationUniteLegale() : ?string;
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * denominationUniteLegale a été modifiée par rapport à la période
	 * précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementDenominationUniteLegale() : bool;
	
	/**
	 * Gets cette variable désigne le nom (ou les noms) sous lequel
	 * l'entreprise est connue du grand public. Cet élément d'identification
	 * de l'entreprise (sur trois champs : denominationUsuelle1UniteLegale,
	 * denominationUsuelle2UniteLegale et denominationUsuelle3UniteLegale) a
	 * été enregistré au niveau unité légale avant l'application de la
	 * norme d'échanges CFE de 2008.
	 * À partir de la norme 2008, la dénomination usuelle est enregistrée au
	 * niveau de l'établissement sur un seul champ :
	 * denominationUsuelleEtablissement. Variables historisées avec une seule
	 * indicatrice de changement pour les trois variables.
	 * 
	 * Longueur : 70
	 * 
	 * @return ?string
	 */
	public function getDenominationUsuelle1UniteLegale() : ?string;
	
	/**
	 * Gets second champ pour la dénomination usuelle.
	 * 
	 * @return ?string
	 */
	public function getDenominationUsuelle2UniteLegale() : ?string;
	
	/**
	 * Gets dernier champ pour la dénomination usuelle.
	 * 
	 * @return ?string
	 */
	public function getDenominationUsuelle3UniteLegale() : ?string;
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * denominationUsuelleUniteLegale a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementDenominationUsuelleUniteLegale() : bool;
	
	/**
	 * Gets la catégorie juridique est un attribut des unités légales.
	 * Cette variable est à 1000 pour les personnes physiques.
	 * Lors de son dépôt de demande de création, le déclarant indique la
	 * forme juridique de l’unité légale qu’il crée, qui est ensuite
	 * traduite en code.
	 * Ce code est modifiable, à la marge, au cours de la vie de l’unité
	 * légale (pour les personnes morales) en fonction des déclarations de
	 * l’exploitant. Pour chaque unité légale, il existe à un instant
	 * donné un seul code Catégorie juridique. Il est attribué selon la
	 * nomenclature en vigueur.
	 * 
	 * *Historique*
	 * La catégorie juridique peut être à null (cas des unités purgées,
	 * première date de début de la catégorie juridique postérieure à la
	 * première date de début d'une autre variable historisée). En revanche,
	 * le libellé associé aux catégories juridiques n’a pas été
	 * historisé, si bien que des codes peuvent paraître « Hors nomenclature
	 * », alors qu’ils ont été valides à un instant donné.
	 * 
	 * Longueur : 4
	 * 
	 * @return ?string
	 */
	public function getCategorieJuridiqueUniteLegale() : ?string;
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * categorieJuridiqueUniteLegale a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementCategorieJuridiqueUniteLegale() : bool;
	
	/**
	 * Gets lors de son inscription au répertoire, l’Insee attribue à toute
	 * unité légale un code dit « APE » sur la base de la description de
	 * l’activité principale faite par le déclarant. Ce code est modifiable
	 * au cours de la vie de l’unité légale en fonction des déclarations
	 * de l’exploitant.
	 * Pour chaque unité légale, il existe à un instant donné un seul code
	 * « APE ». Il est attribué selon la nomenclature en vigueur. La
	 * nomenclature en vigueur est la Naf Rév2 et ce depuis le 1 er Janvier
	 * 2008. Chaque code comporte 2 chiffres, un point, 2 chiffres et une
	 * lettre. Toutes les unités légales actives au 01/01/2008 ont eu leur
	 * code APE recodé dans la nouvelle nomenclature, ainsi de très
	 * nombreuses entreprises ont une période débutant à cette date. Au
	 * moment de la déclaration de l’entreprise, il peut arriver que
	 * l’Insee ne soit pas en mesure d’attribuer le bon code APE : la
	 * modalité 00.00Z peut alors être affectée provisoirement.
	 * 
	 * *Historique*
	 * Le code APE est historisé depuis le 01/01/2005.
	 * La règle d’historisation des données d’activité est la suivante :
	 *  - Pour les entreprises cessées avant le 31/12/2004, seul le dernier
	 * code activité connu figure, dans la nomenclature en vigueur à la date
	 * de fermeture.
	 *  - Pour les entreprises actives après le 01/01/2005 et cessées avant
	 * le 31/12/2007, l’historique des codes attribués sur la période est
	 * disponible.
	 *  - Pour les entreprises actives après le 01/01/2005 et toujours actives
	 * le 1/1/2008, l’historique intègre le changement de nomenclature.
	 *  - Pour les entreprises créées après le 01/01/2008, l’historique
	 * comprend les modifications apportées au cours de la vie de
	 * l’entreprise.
	 * L'APE peut être à null (cas des unités purgées - pour des raisons de
	 * capacité de stockage des données, les données concernant les
	 * entreprises cessées avant le 31/12/2002 ont été purgées -, première
	 * date de début de l'APE postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * Longueur : 6
	 * 
	 * @return ?string
	 */
	public function getActivitePrincipaleUniteLegale() : ?string;
	
	/**
	 * Gets cette variable indique la nomenclature d’activité correspondant
	 * à la variable activitePrincipaleUniteLegale (cf.
	 * activitePrincipaleUniteLegale).
	 * 
	 * @return ?ApiFrInseeSireneNomenclatureApeInterface
	 */
	public function getNomenclatureActivitePrincipaleUniteLegale() : ?ApiFrInseeSireneNomenclatureApeInterface;
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * activitePrincipaleUniteLegale a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementActivitePrincipaleUniteLegale() : bool;
	
	/**
	 * Gets le siège d’une unité légale est le lieu où sont centralisées
	 * l’administration et la direction effective de l’unité légale.
	 * À un instant donné, chaque unité légale a un seul siège. Mais, au
	 * cours de la vie de l’unité légale, le siège peut être différent.
	 * Chaque siège est identifié par un numéro Nic (Numéro Interne de
	 * Classement de l'établissement) qui respecte les règles d’attribution
	 * des numéros d’établissement.
	 * Le Nic est composé de quatre chiffres et d'un cinquième qui permet de
	 * contrôler la validité du numéro Siret (concaténation du numéro
	 * Siren et du Nic).
	 * 
	 * *Historique*
	 * Le Nic du siège peut être à null sur une période mais, en règle
	 * générale, pas sur l’ensemble de l’historique (cas des unités
	 * purgées, première date de début du Nic postérieure à la première
	 * date de début d'une autre variable historisée).
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getNicSiegeUniteLegale() : ?string;
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * nicSiegeUniteLegale a été modifiée par rapport à la période
	 * précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementNicSiegeUniteLegale() : bool;
	
	/**
	 * Gets cette variable indique si l'entreprise appartient au champ de
	 * l’économie sociale et solidaire. La loi n° 2014-856 du 31 juillet
	 * 2014 définit officiellement le périmètre de l’économie sociale et
	 * solidaire (ESS).
	 * Celle-ci comprend les quatre familles traditionnelles en raison de leur
	 * régime juridique (associations, fondations, coopératives et mutuelles)
	 * et inclut une nouvelle catégorie, les entreprises de l’ESS, adhérant
	 * aux mêmes principes :
	 *  - poursuivre un but social autre que le seul partage des bénéfices ;
	 *  - un caractère lucratif encadré (notamment des bénéfices
	 * majoritairement consacrés au maintien et au développement de
	 * l’activité) ;
	 *  - une gouvernance démocratique et participative.
	 * Cette variable est historisée, renseignée pour environ 1 million
	 * d'entreprises, sinon null.
	 * 
	 * @return ?bool
	 */
	public function hasEconomieSocialeSolidaireUniteLegale() : ?bool;
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * economieSocialeSolidaireUniteLegale a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementEconomieSocialeSolidaireUniteLegale() : bool;
	
	/**
	 * Gets lors de sa formalité de création, le déclarant indique si
	 * l’unité légale aura ou non des employés. Par la suite, le
	 * déclarant peut également faire des déclarations de prise d’emploi
	 * et de fin d’emploi. La prise en compte d’une déclaration de prise
	 * d’emploi bascule immédiatement l’unité légale en « employeuse
	 * ». Inversement, lorsqu’une déclaration de fin d’emploi est
	 * traitée, l’unité légale devient « non employeuse ». Le caractère
	 * employeur est O si au moins l'un des établissements actifs de l'unité
	 * légale emploie des salariés.
	 * Cette variable est historisée.
	 * 
	 * @return ?bool
	 */
	public function hasCaractereEmployeurUniteLegale() : ?bool;
	
	/**
	 * Gets c’est une variable booléenne qui indique si la variable
	 * CaractereEmployeurUniteLegale a été modifiée par rapport à la
	 * période précédente.
	 * 
	 * @return bool
	 */
	public function hasChangementCaractereEmployeurUniteLegale() : bool;
	
	/**
	 * Gets est-ce que cette unité légale est une société à mission pour
	 * la période.
	 * 
	 * @return ?bool
	 */
	public function hasSocieteMissionUniteLegale() : ?bool;
	
	/**
	 * Gets est-ce que la variable societeMissionUniteLegale a changé depuis
	 * la dernière période.
	 * 
	 * @return bool
	 */
	public function hasChangementSocieteMissionUniteLegale() : bool;
	
}
