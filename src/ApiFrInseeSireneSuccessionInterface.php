<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use DateTimeInterface;
use Stringable;

/**
 * ApiFrInseeSireneSuccessionInterface interface file.
 * 
 * This represents a line into the InseeSireneEtablissementLiensSuccession
 * file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeSireneSuccessionInterface extends Stringable
{
	
	/**
	 * Gets cette variable désigne le numéro Siret de l’établissement
	 * prédécesseur.
	 * Le numéro Siret est le numéro unique d’identification attribué à
	 * chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * 
	 * Longueur : 14
	 * 
	 * @return string
	 */
	public function getSiretEtablissementPredecesseur() : string;
	
	/**
	 * Gets cette variable désigne le numéro Siret de l’établissement
	 * successeur.
	 * Le numéro Siret est le numéro unique d’identification attribué à
	 * chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * 
	 * Longueur : 14
	 * 
	 * @return string
	 */
	public function getSiretEtablissementSuccesseur() : string;
	
	/**
	 * Gets cette variable indique la date à laquelle la succession a lieu.
	 * 
	 * Longueur : 10
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateLienSuccession() : DateTimeInterface;
	
	/**
	 * Gets c’est une variable booléenne qui indique s’il s’agit d’un
	 * transfert de siège.
	 * 
	 * Il peut s’agir d’un transfert de l’établissement ayant la
	 * qualité de siège, ou d’un transfert de la qualité de siège d’un
	 * établissement à un autre.
	 * La variable prend la valeur true si le lien de succession concerne
	 * l’établissement siège, false si seulement des établissements
	 * secondaires sont concernés.
	 * 
	 * Longueur : 5
	 * 
	 * @return bool
	 */
	public function hasTransfertSiege() : bool;
	
	/**
	 * Gets c’est une variable booléenne qui indique s’il y a continuité
	 * économique entre les deux établissements ou non :
	 *  - true s’il y a continuité économique,
	 *  - false s’il n’y a pas continuité économique,.
	 * 
	 * Notion de continuité économique :
	 * Il y a continuité économique entre deux établissements qui se
	 * succèdent dès lors que deux des trois critères suivants sont
	 * vérifiés :
	 *  - les deux établissements appartiennent à la même unité légale
	 * (même Siren) ;
	 *  - les deux établissements exercent la même activité (même code APE)
	 * ;
	 *  - les deux établissements sont situés dans un même lieu (numéro et
	 * libellé de voie, code commune).
	 * À noter : en cas de transfert de siège, la variable
	 * continuiteEconomique est toujours à true.
	 * 
	 * Longueur : 5
	 * 
	 * @return bool
	 */
	public function hasContinuiteEconomique() : bool;
	
	/**
	 * Gets cette variable indique la date à laquelle le lien de succession a
	 * été enregistré dans le répertoire Sirene.
	 * 
	 * Longueur : 19
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDernierTraitementLienSuccession() : DateTimeInterface;
	
}
