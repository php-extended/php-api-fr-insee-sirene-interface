<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use DateTimeInterface;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoieInterface;
use Stringable;

/**
 * ApiFrInseeSireneEtablissementInterface interface file.
 * 
 * This represents a line into the StockEtablissement file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
interface ApiFrInseeSireneEtablissementInterface extends Stringable
{
	
	/**
	 * Gets un numéro d’identité de l’unité légale est attribué par
	 * l’Insee à toutes les personnes physiques ou morales inscrites au
	 * répertoire ainsi qu’à leurs établissements : le numéro Siren. Ce
	 * numéro unique est « attribué soit à l’occasion des demandes
	 * d’immatriculation au registre du commerce et des sociétés ou des
	 * déclarations effectuées au répertoire des métiers, soit à la
	 * demande d’administrations » (article R123-224 du code de  commerce).
	 * Lors de sa création, une unité légale se voit attribuer un numéro
	 * Siren de 9 chiffres.
	 * 
	 * *Règles de gestion*
	 * Les entrepreneurs individuels, ou personnes physiques, conservent le
	 * même numéro Siren jusqu’à leur décès. Les sociétés, ou
	 * personnes morales, perdent la personnalité juridique au moment de la
	 * cessation de l’activité de l’entreprise. Si l’activité devait
	 * reprendre ultérieurement, un nouveau numéro Siren sera attribué. Les
	 * numéros d’identification sont uniques : lorsqu’un numéro Siren a
	 * été attribué, il ne peut pas être réutilisé et attribué à une
	 * nouvelle unité légale, même lorsque l’activité a cessé.
	 * 
	 * *Historique*
	 * Même si la mise en place du répertoire Sirene remonte à 1973, toutes
	 * les unités légales, y compris celles créées avant cette date,
	 * disposent d’un numéro Siren pour le secteur privé non agricole.
	 * En 1983, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus aux institutions et services de
	 * l’État et aux collectivités territoriales.
	 * En 1993, le champ du répertoire Sirene et l’obligation
	 * d’immatriculation ont été étendus au secteur privé agricole.
	 * 
	 * Longueur : 9
	 * 
	 * @return string
	 */
	public function getSiren() : string;
	
	/**
	 * Gets le numéro interne de classement permet de distinguer les
	 * établissements d'une même entreprise. Il est composé de 5 chiffres.
	 * Associé au Siren, il forme le Siret de l'établissement. Il est
	 * composé de quatre chiffres et d'un cinquième qui permet de contrôler
	 * la validité du numéro Siret.
	 * Le Nic est attribué une seule fois au sein de l'entreprise. Si
	 * l'établissement ferme, son Nic ne peut être réattribué à un nouvel
	 * établissement.
	 * 
	 * Longueur : 5
	 * 
	 * @return string
	 */
	public function getNic() : string;
	
	/**
	 * Gets le numéro Siret est le numéro unique d’identification attribué
	 * à chaque établissement par l’Insee. Ce numéro est un simple numéro
	 * d’ordre, composé de 14 chiffres non significatifs : les neuf premiers
	 * correspondent au numéro Siren de l’entreprise dont l’établissement
	 * dépend et les cinq derniers à un numéro interne de classement (Nic).
	 * Une entreprise est constituée d’autant d’établissements qu’il y
	 * a de lieux différents où elle exerce son activité. L’établissement
	 * est fermé quand l’activité cesse dans l’établissement concerné
	 * ou lorsque l’établissement change d’adresse.
	 * 
	 * Longueur : 14
	 * 
	 * @return string
	 */
	public function getSiret() : string;
	
	/**
	 * Gets seuls les établissements diffusibles sont accessibles à tout
	 * public (statutDiffusionEtablissement=O).
	 * 
	 * @return bool
	 */
	public function hasStatutDiffusionEtablissement() : bool;
	
	/**
	 * Gets la date de création correspond à la date qui figure dans les
	 * statuts de l’entreprise déposés au CFE compétent.
	 * Pour les établissements des unités purgées
	 * (unitePurgeeUniteLegale=true): si la date de création est au 01/01/1900
	 * dans Sirene, la date est forcée à null.
	 * Dans tous les autres cas, la date de création n'est jamais à null. Si
	 * elle est non renseignée, elle sera au 01/01/1900.
	 * La date de création ne correspond pas obligatoirement à dateDebut de
	 * la première période de l'établissement, certaines variables
	 * historisées pouvant posséder des dates de début soit au 1900-01-01,
	 * soit antérieures à la date de création.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateCreationEtablissement() : ?DateTimeInterface;
	
	/**
	 * Gets il s’agit d’une variable statistique, millésimée au 31/12
	 * d’une année donnée (voir variable anneeEffectifsEtablissement).
	 * 
	 * @return ?ApiFrInseeSireneTrancheEffectifsInterface
	 */
	public function getTrancheEffectifsEtablissement() : ?ApiFrInseeSireneTrancheEffectifsInterface;
	
	/**
	 * Gets seule la dernière année de mise à jour de l’effectif salarié
	 * de l’établissement est donnée si celle-ci est inférieure ou égale
	 * à l’année d’interrogation-3. Ainsi une interrogation en 2018 ne
	 * renverra la dernière année de mise à jour de l’effectif que si
	 * cette année est supérieure ou égale à 2015.
	 * 
	 * @return ?int
	 */
	public function getAnneeEffectifsEtablissement() : ?int;
	
	/**
	 * Gets cette variable désigne le code de l'activité exercée par
	 * l'artisan inscrit au registre des métiers selon la Nomenclature
	 * d'Activités Française de l'Artisanat (NAFA).
	 * 
	 * Longueur : 6
	 * 
	 * @return ?string
	 */
	public function getActivitePrincipaleRegistreMetiersEtablissement() : ?string;
	
	/**
	 * Gets cette date peut concerner des mises à jour de données du
	 * répertoire Sirene qui ne sont pas diffusées par API Sirene.
	 * Cette variable peut être à null.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateDernierTraitementEtablissement() : ?DateTimeInterface;
	
	/**
	 * Gets c’est une variable booléenne qui indique si l'établissement est
	 * le siège ou non de l'unité légale. Variable calculée toujours
	 * renseignée.
	 * 
	 * @return bool
	 */
	public function hasEtablissementSiege() : bool;
	
	/**
	 * Gets cette variable donne le nombre de périodes [dateDebut,dateFin] de
	 * l’établissement. Chaque période correspond à l’intervalle de
	 * temps pendant lequel la totalité des variables historisées de
	 * l’établissement n’ont pas été modifiées.
	 * Les dates de ces périodes sont des dates d’effet (et non des dates de
	 * traitement).
	 * 
	 * @return ?int
	 */
	public function getNombrePeriodesEtablissement() : ?int;
	
	/**
	 * Gets cette variable est un élément constitutif de l’adresse.
	 * C'est une variable facultative qui précise l'adresse avec :
	 *  - une indication d'étage, d'appartement, de porte, de N° de boîte à
	 * lettres,
	 *  - la désignation d'un bâtiment, d'un escalier, d'une entrée, d'un
	 * bloc,
	 *  - le nom d'une résidence, d'un ensemble….
	 * 
	 * Longueur : 38
	 * 
	 * @return ?string
	 */
	public function getComplementAdresseEtablissement() : ?string;
	
	/**
	 * Gets c'est un élément constitutif de l’adresse.
	 * Cette variable est facultative.
	 * Si plusieurs numéros de voie sont indiqués (5-7, 5 à 7…),
	 * l'information complète (5-7) ou (5 à 7) figure en complément
	 * d'adresse et le premier des numéros (5 dans l'exemple) est porté dans
	 * la variable numeroVoieEtablissement.
	 * 
	 * @return ?int
	 */
	public function getNumeroVoieEtablissement() : ?int;
	
	/**
	 * Gets cette variable un élément constitutif de l’adresse.
	 * Cette variable est facultative.
	 * Elle doit être associée à la variable numeroVoieEtablissement.
	 * 
	 * Longueur : 1
	 * 
	 * @return ?string
	 */
	public function getIndiceRepetitionEtablissement() : ?string;
	
	/**
	 * Gets le numéro de voie de l'adresse précédente, s'il y en a une..
	 * 
	 * @return ?int
	 */
	public function getDernierNumeroVoieEtablissement() : ?int;
	
	/**
	 * Gets l'indice de répétition de l'adresse précédente, s'il y en a
	 * une.
	 * 
	 * @return ?string
	 */
	public function getIndiceRepetitionDernierNumeroVoieEtablissement() : ?string;
	
	/**
	 * Gets c'est un élément constitutif de l’adresse.
	 * 
	 * Si le type de voie est d'une longueur inférieure ou égale à 4
	 * caractères, le type de voie n'est pas abrégé. Ainsi, RUE ou QUAI sont
	 * écrits tels quels, alors que AVENUE est abrégée en AV.
	 * 
	 * @return ?ApiFrInseeBanTypeVoieInterface
	 */
	public function getTypeVoieEtablissement() : ?ApiFrInseeBanTypeVoieInterface;
	
	/**
	 * Gets cette variable indique le libellé de voie de la commune de
	 * localisation de l'établissement. 
	 * C’est un élément constitutif de l’adresse.
	 * Cette variable est facultative : elle n'est pas toujours renseignée, en
	 * particulier dans les petites communes.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleVoieEtablissement() : ?string;
	
	/**
	 * Gets cette variable désigne le code postal de l’adresse de
	 * l’établissement.
	 * Pour les adresses à l’étranger (codeCommuneEtablissement commence
	 * par 99), cette variable est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodePostalEtablissement() : ?string;
	
	/**
	 * Gets cette variable indique le libellé de la commune de localisation de
	 * l'établissement si celui-ci n’est pas à l’étranger. C’est un
	 * élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCommuneEtablissement() : ?string;
	
	/**
	 * Gets cette variable est un élément constitutif de l’adresse pour les
	 * établissements situés sur le territoire étranger (la variable
	 * codeCommuneEtablissement est vide).
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCommuneEtrangerEtablissement() : ?string;
	
	/**
	 * Gets la distribution spéciale reprend les éléments particuliers qui
	 * accompagnent une adresse de distribution spéciale. C’est un élément
	 * constitutif de l’adresse.
	 * Cette variable est facultative.
	 * 
	 * Longueur 26
	 * 
	 * *Exemples de début de modalités attendues*
	 * BP : Boîte postale
	 * TSA : Tri par service à l'arrivée
	 * LP : Local postal
	 * RP : Référence postale
	 * SP : Secteur postal
	 * CP : Case postale
	 * CE : Case entreprise
	 * CS : Course spéciale
	 * POSTE RESTANTE
	 * CEDEX : Courrier d'entreprise à distribution exceptionnelle
	 * CIDEX : Courrier individuel à distribution exceptionnelle
	 * CASE, NIVEAU : Mots utilisés pour la distribution interne du courrier
	 * à LA DEFENSE
	 * CASIER : Utilisé pour le centre commercial des Quatre Temps
	 * SILIC, SENIA, MAREE, FLEURS… : Utilisés sur le site de Rungis
	 * 
	 * @return ?string
	 */
	public function getDistributionSpecialeEtablissement() : ?string;
	
	/**
	 * Gets cette variable désigne le code de la commune de localisation de
	 * l'établissement, hors adresse à l'étranger.
	 * Le code commune correspond au code commune existant à la date de la
	 * mise à disposition : toute modification du code officiel géographique
	 * est répercutée sur la totalité des établissements (même ceux
	 * fermés) correspondant à ce code commune.
	 * 
	 * Pour les établissements localisés à l'étranger, la variable
	 * codeCommuneEtablissement est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodeCommuneEtablissement() : ?string;
	
	/**
	 * Gets cette variable est un élément constitutif de l’adresse.
	 * Elle est facultative.
	 * 
	 * Longueur : 9
	 * 
	 * @return ?string
	 */
	public function getCodeCedexEtablissement() : ?string;
	
	/**
	 * Gets cette variable indique le libellé correspondant au code cedex de
	 * l'établissement si celui-ci est non null. Ce libellé est le libellé
	 * utilisé dans la ligne 6 d’adresse pour l’acheminement postal.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCedexEtablissement() : ?string;
	
	/**
	 * Gets cette variable désigne le code du pays de localisation de
	 * l'établissement pour les adresses à l'étranger. La variable
	 * codePaysEtrangerEtablissement commence toujours par 99 si elle est
	 * renseignée. Les 3 caractères suivants sont le code du pays étranger.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodePaysEtrangerEtablissement() : ?string;
	
	/**
	 * Gets cette variable indique le libellé du pays de localisation de
	 * l'établissement si celui-ci est à l’étranger. C’est un élément
	 * constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibellePaysEtrangerEtablissement() : ?string;
	
	/**
	 * Gets identifiant de l'adresse
	 * Longueur : 15.
	 * 
	 * @return ?string
	 */
	public function getIdentifiantAdresseEtablissement() : ?string;
	
	/**
	 * Gets abscisse des coordonnees Lambert de l'adresse.
	 * 
	 * @return ?float
	 */
	public function getCoordonneeLambertAbscisseEtablissement() : ?float;
	
	/**
	 * Gets ordonnée des coordonnees Lambert de l'adresse.
	 * 
	 * @return ?float
	 */
	public function getCoordonneeLambertOrdonneeEtablissement() : ?float;
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable est un élément constitutif de l’adresse
	 * secondaire.
	 * C'est une variable facultative qui précise l'adresse secondaire avec :
	 *  - une indication d'étage, d'appartement, de porte, de N° de boîte à
	 * lettres,
	 *  - la désignation d'un bâtiment, d'un escalier, d'une entrée, d'un
	 * bloc,
	 *  - le nom d'une résidence, d'un ensemble….
	 * 
	 * Longueur : 38
	 * 
	 * @return ?string
	 */
	public function getComplementAdresse2Etablissement() : ?string;
	
	/**
	 * Gets c'est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * Si plusieurs numéros de voie sont indiqués (5-7, 5 à 7…),
	 * l'information complète (5-7) ou (5 à 7) figure en complément
	 * d'adresse secondaire et le premier des numéros (5 dans l'exemple) est
	 * porté dans la variable numeroVoie2Etablissement.
	 * 
	 * @return ?int
	 */
	public function getNumeroVoie2Etablissement() : ?int;
	
	/**
	 * Gets cette variable un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative. Elle doit être associée à la variable
	 * numeroVoie2Etablissement.
	 * 
	 * Longueur : 1
	 * 
	 * @return ?string
	 */
	public function getIndiceRepetition2Etablissement() : ?string;
	
	/**
	 * Gets cette variable est un élément constitutif de l’adresse
	 * secondaire.
	 * 
	 * Si le type de voie est d'une longueur inférieure ou égale à 4
	 * caractères, le type de voie n'est pas abrégé. Ainsi, RUE ou QUAI sont
	 * écrits tels quels, alors que AVENUE est abrégée en AV.
	 * 
	 * @return ?ApiFrInseeBanTypeVoieInterface
	 */
	public function getTypeVoie2Etablissement() : ?ApiFrInseeBanTypeVoieInterface;
	
	/**
	 * Gets cette variable indique le libellé de la voie de l’adresse
	 * secondaire de l'établissement.
	 * C’est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative : elle n'est pas toujours renseignée, en
	 * particulier dans les petites communes.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleVoie2Etablissement() : ?string;
	
	/**
	 * Gets cette variable désigne le code postal de l’adresse de
	 * l’établissement.
	 * Pour les adresses à l’étranger (codeCommuneEtablissement commence
	 * par 99), cette variable est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodePostal2Etablissement() : ?string;
	
	/**
	 * Gets cette variable indique le libellé de la commune de l’adresse
	 * secondaire de l'établissement si celui-ci n’est pas à l’étranger.
	 * C’est un élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCommune2Etablissement() : ?string;
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable est un élément constitutif de l’adresse
	 * secondaire pour les établissements situés sur le territoire étranger
	 * (la variable codeCommune2Etablissement est vide).
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCommuneEtranger2Etablissement() : ?string;
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, la distribution spéciale reprend les éléments
	 * particuliers qui accompagnent l’adresse secondaire de distribution
	 * spéciale. C’est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * 
	 * Longueur : 26
	 * 
	 * *Exemples de début de modalités attendues*
	 * BP : Boîte postale
	 * TSA : Tri par service à l'arrivée
	 * LP : Local postal
	 * RP : Référence postale
	 * SP : Secteur postal
	 * CP : Case postale
	 * CE : Case entreprise
	 * CS : Course spéciale
	 * POSTE RESTANTE
	 * CEDEX : Courrier d'entreprise à distribution exceptionnelle
	 * CIDEX : Courrier individuel à distribution exceptionnelle
	 * CASE, NIVEAU : Mots utilisés pour la distribution interne du courrier
	 * à LA DEFENSE
	 * CASIER : Utilisé pour le centre commercial des Quatre Temps
	 * SILIC, SENIA, MAREE, FLEURS… : Utilisés sur le site de Rungis
	 * 
	 * @return ?string
	 */
	public function getDistributionSpeciale2Etablissement() : ?string;
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable désigne le code de la commune de l’adresse
	 * secondaire de l’établissement, hors adresse à l'étranger. Le code
	 * commune correspond au code commune existant à la date de la mise à
	 * disposition : toute modification du code officiel géographique est
	 * répercutée sur la totalité des établissements (même ceux fermés)
	 * correspondant à ce code commune.
	 * Pour les établissements localisés à l'étranger, la variable
	 * codeCommune2Etablissement est à null.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodeCommune2Etablissement() : ?string;
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, c’est un élément constitutif de l’adresse secondaire.
	 * Cette variable est facultative.
	 * 
	 * Longueur : 9
	 * 
	 * @return ?string
	 */
	public function getCodeCedex2Etablissement() : ?string;
	
	/**
	 * Gets cette variable indique le libellé correspondant au code cedex de
	 * l’adresse secondaire de l'établissement si celui-ci est non null. Ce
	 * libellé est le libellé utilisé dans la ligne 6 d’adresse pour
	 * l’acheminement postal.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibelleCedex2Etablissement() : ?string;
	
	/**
	 * Gets dans le cas où l’établissement dispose d’une entrée
	 * secondaire, cette variable désigne le code du pays de localisation de
	 * l'adresse secondaire de l'établissement pour les adresses à
	 * l'étranger. La variable codePaysEtranger2Etablissement commence
	 * toujours par 99 si elle est renseignée. Les 3 caractères suivants sont
	 * le code du pays étranger.
	 * 
	 * Longueur : 5
	 * 
	 * @return ?string
	 */
	public function getCodePaysEtranger2Etablissement() : ?string;
	
	/**
	 * Gets cette variable indique le libellé du pays de localisation de
	 * l’adresse secondaire de l'établissement si celui-ci est à
	 * l’étranger.
	 * C’est un élément constitutif de l’adresse.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getLibellePaysEtranger2Etablissement() : ?string;
	
	/**
	 * Gets date de début de la période au cours de laquelle toutes les
	 * variables historisées de l'établissement restent inchangées.
	 * La date 1900-01-01 signifie : date non déterminée.
	 * dateDebut peut-être vide uniquement pour les établissements des
	 * unités purgées (cf. variable unitePurgeeUniteLegale dans le descriptif
	 * des variables du fichier StockUniteLegale).
	 * La date de début de la période la plus ancienne ne correspond pas
	 * obligatoirement à la date de création de l'établissement, certaines
	 * variables historisées pouvant posséder des dates de début soit au
	 * 1900-01-01 soit antérieures à la date de création.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateDebut() : ?DateTimeInterface;
	
	/**
	 * Gets lors de son inscription au répertoire, un établissement est, sauf
	 * exception, à l’état « Actif ». Le passage à l’état « Fermé
	 * » découle de la prise en compte d’une déclaration de fermeture.
	 * À noter qu’un établissement fermé peut être rouvert.
	 * En règle générale, la première période d’historique d’un
	 * établissement correspond à un etatAdministratifUniteLegale égal à «
	 * Actif ». Toutefois, l'état administratif peut être à null (première
	 * date de début de l'état postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * @return ApiFrInseeSireneEtatAdministratifInterface
	 */
	public function getEtatAdministratifEtablissement() : ApiFrInseeSireneEtatAdministratifInterface;
	
	/**
	 * Gets les trois variables enseigne1Etablissement, enseigne2Etablissement
	 * et enseigne3Etablissement contiennent la ou les enseignes de
	 * l'établissement.
	 * L'enseigne identifie l'emplacement ou le local dans lequel est exercée
	 * l'activité. Un établissement peut posséder une enseigne, plusieurs
	 * enseignes ou aucune.
	 * 
	 * L'analyse des enseignes et de son découpage en trois variables dans
	 * Sirene montre deux cas possibles : soit les 3 champs concernent 3
	 * enseignes bien distinctes, soit ces trois champs correspondent au
	 * découpage de l'enseigne qui est déclarée dans la liasse (sur un seul
	 * champ) avec une continuité des trois champs.
	 * 
	 * *Exemples*
	 * SIRET=53053581400178
	 * "enseigne1Etablissement": "LES SERRURIERS DES YVELINES LES VITRIERS DES
	 * YVELI",
	 * "enseigne2Etablissement": "NES LES CHAUFFAGISTES DES YVELINES LES
	 * PLATRIERS D",
	 * "enseigne3Etablissement": "ES YVELINES LES ELECTRICIENS DES
	 * YVELINES.…"
	 * 
	 * SIRET=05439181800033
	 * "enseigne1Etablissement": "HALTE OCCASIONS",
	 * "enseigne2Etablissement": "OUTRE-MER LOCATION",
	 * "enseigne3Etablissement": "OUTRE-MER TRANSIT".
	 * 
	 * Cette variable est historisée.
	 * 
	 * Longueur : 50
	 * 
	 * @return ?string
	 */
	public function getEnseigne1Etablissement() : ?string;
	
	/**
	 * Gets 2e ligne de l'enseigne.
	 * 
	 * @return ?string
	 */
	public function getEnseigne2Etablissement() : ?string;
	
	/**
	 * Gets 3e ligne de l'enseigne.
	 * 
	 * @return ?string
	 */
	public function getEnseigne3Etablissement() : ?string;
	
	/**
	 * Gets cette variable désigne le nom sous lequel l'établissement est
	 * connu du grand public. Cet élément d'identification de
	 * l'établissement a été enregistré au niveau établissement depuis
	 * l'application de la norme d'échanges CFE de 2008. Avant la norme 2008,
	 * la dénomination usuelle était enregistrée au niveau de l'unité
	 * légale sur trois champs (cf. variables denominationUsuelle1UniteLegale
	 * à denominationUsuelle3UniteLegale dans le descriptif des variables du
	 * fichier StockUniteLegale).
	 * Cette variable est historisée.
	 * 
	 * Longueur : 100
	 * 
	 * @return ?string
	 */
	public function getDenominationUsuelleEtablissement() : ?string;
	
	/**
	 * Gets lors de son inscription au répertoire, l’Insee attribue à tout
	 * établissement un code dit « APE » sur la base de la description de
	 * l’activité principale faite par le déclarant. Ce code est modifiable
	 * au cours de la vie de l’établissement en fonction des déclarations
	 * de l’exploitant.
	 * Pour chaque établissement, il existe à un instant donné un seul code
	 * « APE ». Il est attribué selon la nomenclature en vigueur. La
	 * nomenclature en vigueur est la Naf Rév2 et ce depuis le 1 er Janvier
	 * 2008.
	 * Chaque code comporte 2 chiffres, un point, 2 chiffres et une lettre.
	 * Tous les établissements actifs au 01/01/2008 ont eu leur code APE
	 * recodé dans la nouvelle nomenclature, ainsi de très nombreux
	 * établissements ont une période débutant à cette date.
	 * Au moment de la déclaration de l’entreprise, il peut arriver que
	 * l’Insee ne soit pas en mesure d’attribuer le bon code APE : la
	 * modalité 00.00Z peut alors être affectée provisoirement.
	 * 
	 * *Historique*
	 * Le code APE est historisé depuis le 01/01/2005.
	 * La règle d’historisation des données d’activité est la suivante :
	 *  - Pour les établissements fermés avant le 31/12/2004, seul le dernier
	 * code activité connu figure, dans la nomenclature en vigueur à la date
	 * de fermeture.
	 *  - Pour les établissements ouverts après le 01/01/2005 et fermés
	 * avant le 31/12/2007, l’historique des codes attribués sur la période
	 * est disponible.
	 *  - Pour les établissements ouverts après le 01/01/2005 et toujours
	 * ouverts le 01/01/2008, l’historique intègre le changement de
	 * nomenclature.
	 *  - Pour les établissements ouverts après le 01/01/2008, l’historique
	 * comprend les modifications apportées au cours de la vie de
	 * l’établissement.
	 * L'APE peut être à null (cas des unités purgées - pour des raisons de
	 * capacité de stockage des données, les données concernant les
	 * entreprises cessées avant le 31/12/2002 ont été purgées -, première
	 * date de début de l'APE postérieure à la première date de début
	 * d'une autre variable historisée).
	 * 
	 * Longueur : 6
	 * 
	 * @return ?string
	 */
	public function getActivitePrincipaleEtablissement() : ?string;
	
	/**
	 * Gets cette variable indique la nomenclature d’activité correspondant
	 * à la variable activitePrincipaleEtablissement (cf.
	 * activitePrincipaleEtablissement).
	 * 
	 * @return ?ApiFrInseeSireneNomenclatureApeInterface
	 */
	public function getNomenclatureActivitePrincipaleEtablissement() : ?ApiFrInseeSireneNomenclatureApeInterface;
	
	/**
	 * Gets lors de sa formalité d’ouverture, le déclarant indique si
	 * l’établissement aura ou non des employés. Par la suite, le
	 * déclarant peut également faire des déclarations de prise d’emploi
	 * et de fin d’emploi. La prise en compte d’une déclaration de prise
	 * d’emploi bascule immédiatement l’établissement en « employeur ».
	 * Inversement, lorsqu’une déclaration de fin d’emploi est traitée,
	 * l’établissement devient « non employeur ». Pour chaque
	 * établissement, il existe à un instant donné un seul code « employeur
	 * ».
	 * Cette variable est historisée pour les établissements qui étaient
	 * ouverts en 2005 et pour ceux ouverts ultérieurement.
	 * 
	 * @return ?bool
	 */
	public function hasCaractereEmployeurEtablissement() : ?bool;
	
}
