<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use Stringable;

/**
 * ApiFrInseeSireneEtatAdministratifInterface interface file.
 * 
 * This represents the administrative state of the legal unit.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrInseeSireneEtatAdministratifInterface extends Stringable
{
	
	/**
	 * Gets the id of the administrative state.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the code of the administrative state.
	 * 
	 * @return string
	 */
	public function getCode() : string;
	
	/**
	 * Gets the name of the administrative state.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
}
