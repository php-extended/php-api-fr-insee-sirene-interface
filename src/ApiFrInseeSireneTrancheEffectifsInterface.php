<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use Stringable;

/**
 * ApiFrInseeSireneTrancheEffectifsInterface interface file.
 * 
 * This represents the position of the number of people relative to the other
 * entreprises.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrInseeSireneTrancheEffectifsInterface extends Stringable
{
	
	/**
	 * Gets the identifier of the tranche.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the code of the tranche.
	 * 
	 * @return string
	 */
	public function getCode() : string;
	
	/**
	 * Gets the name of the tranche.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the effectif min of the tranche.
	 * 
	 * @return int
	 */
	public function getEffectifMin() : int;
	
	/**
	 * Gets the effectif max of the tranche.
	 * 
	 * @return int
	 */
	public function getEffectifMax() : int;
	
}
