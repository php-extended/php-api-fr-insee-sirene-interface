<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use Stringable;

/**
 * ApiFrInseeSireneGenderInterface interface file.
 * 
 * This represents the gender of a physical person.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeSireneGenderInterface extends Stringable
{
	
	/**
	 * Gets the id of the gender.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the code of the gender.
	 * 
	 * @return string
	 */
	public function getCode() : string;
	
	/**
	 * Gets the name of the gender.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
}
