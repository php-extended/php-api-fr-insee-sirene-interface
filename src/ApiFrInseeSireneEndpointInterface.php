<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-sirene-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeSirene;

use DateTimeInterface;
use Iterator;
use RuntimeException;
use Stringable;

/**
 * ApiFrInseeSireneEndpointInterface class file.
 * 
 * This class represents the driver to get bulk files from the insee (which
 * are deposed on data.gouv.fr).
 * 
 * @author Anastaszor
 */
interface ApiFrInseeSireneEndpointInterface extends Stringable
{
	
	/**
	 * Gets the date of last upload for the searched files.
	 * 
	 * @return DateTimeInterface
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getLatestUploadDate() : DateTimeInterface;
	
	/**
	 * Gets the iterator for the sirene unites legales.
	 * 
	 * @return Iterator<integer, ApiFrInseeSireneUniteLegaleInterface>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getLatestStockUniteLegaleIterator() : Iterator;
	
	/**
	 * Gets the iterator for the sirene unite legale historic.
	 * 
	 * @return Iterator<integer, ApiFrInseeSireneUniteLegaleHistoriqueInterface>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getLatestStockUniteLegaleHistoricIterator() : Iterator;
	
	/**
	 * Gets the iterator for the sirene stock etablissements.
	 * 
	 * @return Iterator<integer, ApiFrInseeSireneEtablissementInterface>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getLatestStockEtablissementIterator() : Iterator;
	
	/**
	 * Gets the iterator for the sirene stock etablissement historic.
	 * 
	 * @return Iterator<integer, ApiFrInseeSireneEtablissementHistoriqueInterface>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getLatestStockEtablissementHistoricIterator() : Iterator;
	
	/**
	 * Gets the iterator for the sirene stock etablissement successions.
	 * 
	 * @return Iterator<integer, ApiFrInseeSireneSuccessionInterface>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getLatestStockSuccessionIterator() : Iterator;
	
	/**
	 * Gets the iterator for the categorie entreprises.
	 * 
	 * @return Iterator<integer, ApiFrInseeSireneCategorieEntrepriseInterface>
	 */
	public function getCategorieEntrepriseIterator() : Iterator;
	
	/**
	 * Gets the iterator for the etat administratifs.
	 * 
	 * @return Iterator<integer, ApiFrInseeSireneEtatAdministratifInterface>
	 */
	public function getEtatAdministratifIterator() : Iterator;
	
	/**
	 * Gets the iterator for the genders.
	 * 
	 * @return Iterator<integer, ApiFrInseeSireneGenderInterface>
	 */
	public function getGenderIterator() : Iterator;
	
	/**
	 * Gets the iterator for the used nomenclature ape.
	 * 
	 * @return Iterator<integer, ApiFrInseeSireneNomenclatureApeInterface>
	 */
	public function getNomenclatureApeIterator() : Iterator;
	
	/**
	 * Gets the iterator for the tranche effectifs.
	 * 
	 * @return Iterator<integer, ApiFrInseeSireneTrancheEffectifsInterface>
	 */
	public function getTrancheEffectifIterator() : Iterator;
	
}
